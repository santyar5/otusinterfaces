﻿using System;
using static System.Console;

namespace ConsoleApp1
{
    public interface IAlgorithm<T>
    {
        int BinarySearch(GenericList<T> array, T searchedValue);

        //TODO
        //MyList MySort(MyList array);
    }

    //for test
    public class Foo
    {
        public Foo() { }
    }
    class Program
    {
        public static void Main (string[] args)
        {
            GenericList<int> intList = new GenericList<int>();
            intList.Add(1);
            intList.Add(2);
            intList.Add(3);
            intList.Add(4);
            intList.Add(5);

            //работает только в отсортированном списке
            WriteLine(intList.BinarySearch(intList, 4)); //как передавать коллекцию через this???
            ReadLine();
        }

    }
}

