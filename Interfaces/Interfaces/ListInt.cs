﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;

namespace ConsoleApp1
{
    public class Node<T>
    {
        public Node(T data)
        {
            Data = data;
        }
        public T Data { get; set; }
        public Node<T> Next { get; set; }
    }
    public class GenericList<T> : IEnumerable<T>, IAlgorithm<T>  // кольцевой связный список
    {
        Node<T> head; // головной/первый элемент
        Node<T> tail; // последний/хвостовой элемент
        int count;  // количество элементов в списке

        // добавление элемента
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            // если список пуст
            if (head == null)
            {
                head = node;
                tail = node;
                tail.Next = head;
            }
            else
            {
                node.Next = head;
                tail.Next = node;
                tail = node;
            }
            count++;
        }
        public bool Remove(T data)
        {
            Node<T> current = head;
            Node<T> previous = null;

            if (IsEmpty) return false;

            do
            {
                if (current.Data.Equals(data))
                {
                    // Если узел в середине или в конце
                    if (previous != null)
                    {
                        // убираем узел current, теперь previous ссылается не на current, а на current.Next
                        previous.Next = current.Next;

                        // Если узел последний,
                        // изменяем переменную tail
                        if (current == tail)
                            tail = previous;
                    }
                    else // если удаляется первый элемент
                    {

                        // если в списке всего один элемент
                        if (count == 1)
                        {
                            head = tail = null;
                        }
                        else
                        {
                            head = current.Next;
                            tail.Next = current.Next;
                        }
                    }
                    count--;
                    return true;
                }

                previous = current;
                current = current.Next;
            } while (current != head);

            return false;
        }

        public int Count { get { return count; } }
        public bool IsEmpty { get { return count == 0; } }

        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
        }

        public bool Contains(T data)
        {
            Node<T> current = head;
            if (current == null) return false;
            do
            {
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            while (current != head);
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            Node<T> current = head;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != head);
        }

        //public IEnumerator<T> GetEnumerator()
        //{
        //    // Перебираем все элементы связного списка, для представления в виде коллекции элементов.
        //    var current = head;
        //    while (current != null)
        //    {
        //        yield return current.Data;
        //        current = current.Next;
        //    }
        //}

        //метод бинарного поиска с использованием цикла
        public int BinarySearch(GenericList<T> array, T searchedValue)
        {
            int left = 0;
            int right = array.count - 1;

            Type type = searchedValue.GetType();
            var method = type.GetMethod("CompareTo", new Type[1] { typeof(object) });

            if (method == null)
            {
                throw new ArgumentException("Not supported type.");
            }
            //пока не сошлись границы массива
            while (left <= right)
            {
                var enumerator = array.GetEnumerator();
                //индекс среднего элемента
                var middle = (left + right) / 2;
                int i = 0;
                while (i <= middle)
                {
                    enumerator.MoveNext();
                    i++;
                }

                var comparedValue = method.Invoke(searchedValue, new object[1] { enumerator.Current });

                if ((int)comparedValue == 0)
                {
                    return middle;
                }
                else if ((int)comparedValue < 0)
                {
                    //сужаем рабочую зону массива с правой стороны
                    right = middle - 1;
                }
                else
                {
                    //сужаем рабочую зону массива с левой стороны
                    left = middle + 1;
                }
            }

            return -1;
        }

        //public static bool IsNumeric(ValueType value)
        //{
        //    return (value is Byte ||
        //            value is Int16 ||
        //            value is Int32 ||
        //            value is Int64 ||
        //            value is SByte ||
        //            value is UInt16 ||
        //            value is UInt32 ||
        //            value is UInt64 ||
        //            value is BigInteger ||
        //            value is Decimal ||
        //            value is Double ||
        //            value is Single ||
        //            value is float);
        //}
    }
}

